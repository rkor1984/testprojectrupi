﻿using System;

namespace Coinbase.SystemTests.ExchangeRates
{
    public class BaseTest : IDisposable
    {
        protected readonly AppsettingsProvider _appSettingsProvider = new AppsettingsProvider();
        public string Url => _appSettingsProvider.Get("Url");
        public void Dispose()
        {

        }
    }
}
