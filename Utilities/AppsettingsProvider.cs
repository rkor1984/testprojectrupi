﻿using Microsoft.Extensions.Configuration;

namespace Coinbase.SystemTests.ExchangeRates
{
    public class AppsettingsProvider
    {
        private readonly IConfiguration _config;

        public AppsettingsProvider()
        {
            _config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();
        }

        public string Get(string name)
        {
            return _config[name];
        }

    }
}
