﻿using Moq;
using Moq.Protected;
using RichardSzalay.MockHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Coinbase.Tests.ExchangeRates
{
    public class MockRestTests
    {
        [Fact]
        public async Task PostHttpMockTest()
        {
            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(@"[1]"),
            };
            handlerMock.Protected()
                       .Setup<Task<HttpResponseMessage>>(
                             "SendAsync",
                             ItExpr.IsAny<HttpRequestMessage>(),
                             ItExpr.IsAny<CancellationToken>())
                       .ReturnsAsync(response);


            var httpClient = new HttpClient(handlerMock.Object);

            var res =await httpClient.PostAsync("http://test/url",null);

            Assert.True(res.IsSuccessStatusCode);
        }
    }
}
