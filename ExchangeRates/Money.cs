﻿using System.Collections.Generic;

namespace Coinbase.SystemTests.ExchangeRates
{
    public class Cuurrency
    {
        public string currency { get; set; }
        public string amount { get; set; }
        public Dictionary<string,string> rates { get; set; }
    }

    public class Money
    {
        public Cuurrency data { get; set; }
    }
}
