﻿
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using System.Text.Json;

namespace Coinbase.SystemTests.ExchangeRates
{
    public class ExchangeRatesTests: BaseTest
    {
        private HttpClient restClient = new HttpClient();
        [Fact]
        public async Task ExchangeRateGetTest()
        {
            var response = await restClient.GetAsync(new Uri(Url+ "exchange-rates"));
            Assert.True(response.IsSuccessStatusCode);
        }

        [Fact]
        public async Task ExchangeRateGetForBtcTest()
        {
            var response = await restClient.GetAsync(new Uri(Url + "exchange-rates?currency=BTC"));
            Assert.True(response.IsSuccessStatusCode);
          
            string json = await response.Content.ReadAsStringAsync();
            var result = JsonSerializer.Deserialize<Money>(json);

            Assert.Equal("BTC", result.data.currency);
            Assert.Equal(283, result.data.rates.Keys.Count);
        }
        
        [Theory]
        [InlineData("BTC","USD", "sell")]
        [InlineData("BTC","USD", "buy")]
        [InlineData("BTC", "USD", "spot")]
        public async Task PricesForPairTest(string currency1,string currency2,string priceType)
        {
            var response = await restClient.GetAsync(new Uri(Url + "prices/"+ currency1+ "-"+currency2+ "/"+ priceType));
            Assert.True(response.IsSuccessStatusCode);
          
            string json = await response.Content.ReadAsStringAsync();
            var result = JsonSerializer.Deserialize<Money>(json);

            Assert.Equal(currency2, result.data.currency);
            Assert.NotNull(result.data.amount);
        }
    }

 
}
